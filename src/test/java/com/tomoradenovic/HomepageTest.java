package com.tomoradenovic;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class HomepageTest  extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public HomepageTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( HomepageTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testHomepage()
    {

        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");

        ChromeOptions chromeOptions = new ChromeOptions();


        // We get the value of "my_headless" system property.
        // In order to run in headless mode, run maven command as follows:
        // mvn -Dmy_headless=true test
        String headless = System.getProperty("my_headless", "false");

        System.out.println("value of my_headless is: " + headless);

        if (headless.equals("true")) {
            chromeOptions.addArguments("--headless");
        }

        //Step 1- Driver Instantiation: Instantiate driver object as FirefoxDriver
        WebDriver driver = new ChromeDriver(chromeOptions);

        //Step 2- Navigation: Open a website
        driver.navigate().to("https://www.winwin.rs/");

        //Step 3- Assertion: Check its title is correct
        //assertEquals method Parameters: Expected Value, Actual Value, Assertion Message
        assertEquals("Title check failed!", "Online prodaja i isporuka - shop u Vašem mestu - WinWin", driver.getTitle());

        //Step 4- Close Driver
        driver.close();

        //Step 5- Quit Driver
        driver.quit();
    }
}